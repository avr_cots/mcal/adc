/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 2nd, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			ADC									*/
/*   Version :          V01	                                */
/*	 File	 :			config.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef ADC_CONFIG_H_
#define ADC_CONFIG_H_


#define ADC_u8_REF_VOLT						ADC_u8_REF_VOLT_ADC_AVCC
#define ADC_u8_ADJUSTMENT					ADC_u8_ADJUSTMENT_LEFT
#define ADC_u8_STATE						ADC_u8_STATE_ENABLED
#define ADC_u8_AUTO_TRIG_MODE				ADC_u8_AUTO_TRIG_MODE_OFF
#define ADC_u8_INTERRUPT_STATE				ADC_u8_INTERRUPT_STATE_DISABLED
#define ADC_u8_PRESCALER					ADC_u8_PRESCALER_2
#define ADC_u8_AUTO_TRIG_SRC				ADC_u8_AUTO_TRIG_SRC_FREE


/*	Channels and Array of results for Group 1	*/
u8 Group1[ADC_u8_GROUP1_NO_OF_CHANNELS] = {ADC_u8_CHANNEL_3, ADC_u8_CHANNEL_2};
u8 Results1[ADC_u8_GROUP1_NO_OF_CHANNELS] = {10, 20};

/*	Callback functions and Flags for Groups		*/
PtrToCallbackFunctionType ADC_CallbackFunctions[ADC_u8_NO_OF_GROUPS] = {NULL};
u8 ADC_u8Flags[ADC_u8_NO_OF_GROUPS] = {0};

/*	Configurations for each group				*/
ADC_Group ADC_AGroups[ADC_u8_NO_OF_GROUPS] =
{
		{
				ADC_u8_GROUP1_NO_OF_CHANNELS,
				Group1,
				Results1,
				ADC_u8_AUTO_TRIG_SRC_FREE
		}
};



#endif /* ADC_CONFIG_H_ */
