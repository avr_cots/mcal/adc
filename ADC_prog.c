/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 2nd, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			ADC									*/
/*   Version :          V01	                                */
/*	 File	 :			prog.c								*/
/*                                                          */
/* ******************************************************** */


#include "STD_TYPES.h"
#include "BIT_CALC.h"

#include "ADC_interface.h"
#include "ADC_private.h"
#include "ADC_config.h"

static u8 ADC_u8GroupNumber;


void ADC_voidInit(void)
{
	/*	Reference Voltage			*/
	ADC_u8_ADMUX |= ADC_u8_REF_VOLT;
	/*	Left of right Adjustment	*/
	ADC_u8_ADMUX |= ADC_u8_ADJUSTMENT;
	/*	Prescaler					*/
	ADC_u8_ADCSRA |= ADC_u8_PRESCALER;
	/*	Interrupt Enabled/Disabled	*/
	ADC_u8_ADCSRA |= ADC_u8_INTERRUPT_STATE;
	/*	Enabling/Disabling ADC		*/
	ADC_u8_ADCSRA |= ADC_u8_STATE;

	return;
}


u8 ADC_u8GetADCBlocking(u8 Copy_u8ChannelNumber, u8 *Copy_Pu8Reading)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Input Validation	*/
	if( (Copy_u8ChannelNumber < ADC_u8_MAX_CHANNELS_NUM) && (Copy_Pu8Reading != NULL) )
	{
		/*	Selecting the ADC channel									*/
		ADC_u8_ADMUX |= Copy_u8ChannelNumber;
		/*	Start Conversion											*/
		SET_BIT(ADC_u8_ADCSRA, 6);
		/*	Flag Polling 												*/
		while(GET_BIT(ADC_u8_ADCSRA, 4)!= 1);
		/*	Reading the ADCH register if the reading is left-adjusted	*/
		if(ADC_u8_ADJUSTMENT == ADC_u8_ADJUSTMENT_LEFT)
		{
			*Copy_Pu8Reading = ADC_u8_ADCH;
		}

		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}


u8 ADC_u8Refresh(u8 Copy_u8GroupNumber)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Input Validation	*/
	if(Copy_u8GroupNumber < ADC_u8_NO_OF_GROUPS)
	{
		/*	This global variable passes the group index to the ISR		*/
		ADC_u8GroupNumber = Copy_u8GroupNumber;

		/*	Checking whether this is the first conversion				*/
		if(ADC_u8Flags[Copy_u8GroupNumber] == 0)
		{
			/*	Selecting the channel, using the array of groups		*/
			ADC_u8_ADMUX |= ADC_AGroups[Copy_u8GroupNumber].PtrToChannels[0];

			/*	If the Auto Trigger feature is disabled for this group	*/
			if(ADC_AGroups[Copy_u8GroupNumber].TriggerSource == ADC_u8_AUTO_TRIG_SRC_FREE)
			{
				/*	Start Conversion									*/
				SET_BIT(ADC_u8_ADCSRA, 6);
			}
			/*	If the Auto Trigger feature is enabled for this group	*/
			else
			{
				/*	Auto Trigger Enable									*/
				SET_BIT(ADC_u8_ADCSRA, 5);
				/*	Selecting the Trigger source						*/
				ADC_u8_SFIOR |= ADC_AGroups[Copy_u8GroupNumber].TriggerSource;
			}

		}

		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}


void __vector_16(void)
{
	/*	Saving the ADC reading	*/
	ADC_AGroups[ADC_u8GroupNumber].PtrToResults[ADC_u8Flags[ADC_u8GroupNumber]] = ADC_u8_ADCH;
	//Results1[ADC_u8Flags[ADC_u8GroupNumber]] = ADC_u8_ADCH;
	/*	Incrementing the flag	*/
	ADC_u8Flags[ADC_u8GroupNumber]++;
	/*	Checking whether there are more channels in this group	*/
	if(ADC_u8Flags[ADC_u8GroupNumber] < ADC_AGroups[ADC_u8GroupNumber].NoOfChannels)
	{
		/*	Clearing interrupt flag		*/
		SET_BIT(ADC_u8_ADCSRA, 4);

		/*	Select the next channel		*/
		CLR_BIT(ADC_u8_ADMUX, 0);
		CLR_BIT(ADC_u8_ADMUX, 1);
		CLR_BIT(ADC_u8_ADMUX, 2);
		ADC_u8_ADMUX |= ADC_AGroups[ADC_u8GroupNumber].PtrToChannels[ADC_u8Flags[ADC_u8GroupNumber]];

		/*	Start Conversion									*/
		SET_BIT(ADC_u8_ADCSRA, 6);
	}
	else if(ADC_u8Flags[ADC_u8GroupNumber] == ADC_AGroups[ADC_u8GroupNumber].NoOfChannels)
	{
		/*	Clearing interrupt flag			*/
		SET_BIT(ADC_u8_ADCSRA, 4);

		/*	Calling the callback function	*/
		ADC_CallbackFunctions[ADC_u8GroupNumber]();

		/*	Resetting the flag				*/
		ADC_u8Flags[ADC_u8GroupNumber] = 0;

	}


}


u8 ADC_u8GetADCNonBlocking(u8 Copy_u8GroupIndex, u8 Copy_u8ChannelIndex, u8 *Copy_Pu8Reading)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Input Validation	*/
	if( (Copy_u8GroupIndex < ADC_u8_NO_OF_GROUPS) && (Copy_Pu8Reading != NULL) )
	{
		*Copy_Pu8Reading =  ADC_AGroups[Copy_u8GroupIndex].PtrToResults[Copy_u8ChannelIndex];

		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}


u8 ADC_u8SetCallback(PtrToCallbackFunctionType Copy_CallbackFunctionPtr)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Input Validation	*/
	if(Copy_CallbackFunctionPtr != NULL)
	{
		ADC_CallbackFunctions[ADC_u8GroupNumber] = Copy_CallbackFunctionPtr;

	}
	return Local_u8Error;
}


u8 ADC_u8Enable(void)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Checking if the ADC is already enabled	*/
	if(GET_BIT(ADC_u8_ADCSRA, 7) == 0)
	{
		/*	Enabling the ADC					*/
		SET_BIT(ADC_u8_ADCSRA, 7);

		Local_u8Error = STD_u8_OK;
	}
	return Local_u8Error;
}


u8 ADC_u8Disable(void)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Checking if the ADC is already disabled	*/
	if(GET_BIT(ADC_u8_ADCSRA, 7)!= 0)
	{
		/*	Disabling the ADC					*/
		CLR_BIT(ADC_u8_ADCSRA, 7);

		Local_u8Error = STD_u8_OK;
	}
	return Local_u8Error;
}


u8 ADC_u8InterruptEnable(void)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Checking if the ADC Interrupt is already enabled	*/
	if(GET_BIT(ADC_u8_ADCSRA, 3) == 0)
	{
		/*	Enabling the ADC Interrupt						*/
		SET_BIT(ADC_u8_ADCSRA, 3);

		Local_u8Error = STD_u8_OK;
	}
	return Local_u8Error;
}


u8 ADC_u8InterruptDisable(void)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Checking if the ADC Interrupt is already disabled	*/
	if(GET_BIT(ADC_u8_ADCSRA, 3) != 0)
	{
		/*	Disabling the ADC Interrupt						*/
		CLR_BIT(ADC_u8_ADCSRA, 3);

		Local_u8Error = STD_u8_OK;
	}
	return Local_u8Error;
}

