/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 2nd, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			ADC									*/
/*   Version :          V01	                                */
/*	 File	 :			interface.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef ADC_INTERFACE_H_
#define ADC_INTERFACE_H_

typedef void (*PtrToCallbackFunctionType)(void);

void ADC_voidInit(void);
u8 ADC_u8GetADCBlocking(u8 Copy_u8ChannelNumber, u8 *Copy_Pu8Reading);
u8 ADC_u8Refresh(u8 Copy_u8GroupNumber);
void __vector_16(void) __attribute__ ((signal));
u8 ADC_u8GetADCNonBlocking(u8 Copy_u8GroupIndex, u8 Copy_u8ChannelIndex, u8 *Copy_Pu8Reading);
u8 ADC_u8SetCallback(PtrToCallbackFunctionType Copy_CallbackFunctionPtr);
u8 ADC_u8Enable(void);
u8 ADC_u8Disable(void);
u8 ADC_u8InterruptEnable(void);
u8 ADC_u8InterruptDisable(void);


#define ADC_u8_CHANNEL_0					(u8)0x00
#define ADC_u8_CHANNEL_1                    (u8)0x01
#define ADC_u8_CHANNEL_2                    (u8)0x02
#define ADC_u8_CHANNEL_3                    (u8)0x03
#define ADC_u8_CHANNEL_4                    (u8)0x04
#define ADC_u8_CHANNEL_5                    (u8)0x05
#define ADC_u8_CHANNEL_6                    (u8)0x06
#define ADC_u8_CHANNEL_7                    (u8)0x07

#define ADC_u8_AUTO_TRIG_SRC_FREE			(u8)0x00
#define ADC_u8_AUTO_TRIG_SRC_ANALOG_COMP	(u8)0x20
#define ADC_u8_AUTO_TRIG_SRC_EXTI0			(u8)0x40
#define ADC_u8_AUTO_TRIG_SRC_TIMER0_CM		(u8)0x60
#define ADC_u8_AUTO_TRIG_SRC_TIMER0_OV		(u8)0x80
#define ADC_u8_AUTO_TRIG_SRC_TIMER1_CMB		(u8)0xA0
#define ADC_u8_AUTO_TRIG_SRC_TIMER1_OV		(u8)0xC0
#define ADC_u8_AUTO_TRIG_SRC_TIMER1_ICU		(u8)0xE0


#define ADC_u8_NO_OF_GROUPS					(u8)1

#define ADC_u8_GROUP1_NO_OF_CHANNELS		(u8)2



typedef struct
{
	u8 NoOfChannels;
	u8 *PtrToChannels;
	u8 *PtrToResults;
	u8 TriggerSource;

}ADC_Group;

extern u8 Results1[ADC_u8_GROUP1_NO_OF_CHANNELS];




#endif /* ADC_INTERFACE_H_ */
