/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 2nd, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			ADC									*/
/*   Version :          V01	                                */
/*	 File	 :			private.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef ADC_PRIVATE_H_
#define ADC_PRIVATE_H_

#define ADC_u8_ADMUX						*((u8*)0x27)
#define ADC_u8_ADCSRA						*((u8*)0x26)
#define ADC_u8_ADCH							*((u8*)0x25)
#define ADC_u8_ADCL							*((u8*)0x24)
#define ADC_u8_SFIOR						*((u8*)0x50)


#define ADC_u8_MAX_CHANNELS_NUM				(u8)8


#define ADC_u8_REF_VOLT_EXTERNAL_VCC		(u8)0x00
#define ADC_u8_REF_VOLT_ADC_AVCC			(u8)0x40
#define ADC_u8_REF_VOLT_INTERNAL_VCC		(u8)0xC0

#define ADC_u8_ADJUSTMENT_LEFT				(u8)0x20
#define ADC_u8_ADJUSTMENT_RIGHT				(u8)0x00

#define ADC_u8_STATE_ENABLED				(u8)0x80
#define ADC_u8_STATE_DISABLED				(u8)0x00

#define ADC_u8_AUTO_TRIG_MODE_ON			(u8)0x20
#define ADC_u8_AUTO_TRIG_MODE_OFF			(u8)0x00

#define ADC_u8_INTERRUPT_STATE_ENABLED		(u8)0x08
#define ADC_u8_INTERRUPT_STATE_DISABLED		(u8)0x00

#define ADC_u8_PRESCALER_2                  (u8)0x00
#define ADC_u8_PRESCALER_4                  (u8)0x02
#define ADC_u8_PRESCALER_8                  (u8)0x03
#define ADC_u8_PRESCALER_16                 (u8)0x04
#define ADC_u8_PRESCALER_32                 (u8)0x05
#define ADC_u8_PRESCALER_64                 (u8)0x06
#define ADC_u8_PRESCALER_128                (u8)0x07

#define ADC_u8_AUTO_TRIG_SRC_FREE			(u8)0x00
#define ADC_u8_AUTO_TRIG_SRC_ANALOG_COMP	(u8)0x20
#define ADC_u8_AUTO_TRIG_SRC_EXTI0			(u8)0x40
#define ADC_u8_AUTO_TRIG_SRC_TIMER0_CM		(u8)0x60
#define ADC_u8_AUTO_TRIG_SRC_TIMER0_OV		(u8)0x80
#define ADC_u8_AUTO_TRIG_SRC_TIMER1_CMB		(u8)0xA0
#define ADC_u8_AUTO_TRIG_SRC_TIMER1_OV		(u8)0xC0
#define ADC_u8_AUTO_TRIG_SRC_TIMER1_ICU		(u8)0xE0


#endif /* ADC_PRIVATE_H_ */
