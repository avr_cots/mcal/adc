/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 2nd, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			ADC									*/
/*   Version :          V01	                                */
/*	 File	 :			main.c	(App Layer)					*/
/*                                                          */
/* ******************************************************** */

#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "DELAY.h"

#include "DIO_interface.h"
#include "ADC_interface.h"
#include "LCD_interface.h"
#include "MCU_interface.h"


u8 ADC_value;
u8 voltage_string[] = "Voltage: ";
u8 ADC_value_ISR;

u16 LCD_value1;
u16 LCD_value2;

u8 Temp;

u8 LCD_string1[]= "0000";
u8 LCD_string2[]= "0000";

void Callback_Func(void);


int main(void)
{
	DIO_voidInit();

	MCU_u8EnableGlobalInterrupt();
	//This is to test the Non-blocking API
	/**/
	ADC_u8SetCallback(Callback_Func);
	ADC_u8InterruptEnable();
	/**/
	ADC_voidInit();
	LCD_voidInit();
	LCD_voidClear();

	LCD_u8Write(voltage_string, 0, 0);

	while(1)
	{
		ADC_u8Refresh(0);
		/*
		// Testing the Blocking API
		ADC_u8GetADCBlocking(1, &ADC_value);
		LCD_value1 = 19.5 * ADC_value;

		LCD_string1[3] = (LCD_value1  % 10) + '0';
		LCD_string1[2] = ((LCD_value1 / 10)%10) + '0';
		LCD_string1[1] = ((LCD_value1 / 100)%10) + '0';
		LCD_string1[0] = ((LCD_value1 / 1000)%10) + '0';

		delay_milliseconds(100);
		LCD_u8Write(LCD_string1, 0, 1);
		 */

	}

	return 0;
}


void Callback_Func(void)
{
	ADC_u8GetADCNonBlocking(0, 0, &Temp);
	LCD_value1 = 19.5 * Temp;
	ADC_u8GetADCNonBlocking(0, 1, &Temp);
	LCD_value2 = 19.5 * Temp;

	//LCD_value1 = 19.5 * Results1[0];
	//LCD_value2 = 19.5 * Results1[1];

	LCD_string1[3] = (LCD_value1  % 10) + '0';
	LCD_string1[2] = ((LCD_value1 / 10)%10) + '0';
	LCD_string1[1] = ((LCD_value1 / 100)%10) + '0';
	LCD_string1[0] = ((LCD_value1 / 1000)%10) + '0';

	delay_milliseconds(20);
	LCD_u8Write(LCD_string1, 0, 1);

	/*****/


	LCD_string2[3] = (LCD_value2  % 10) + '0';
	LCD_string2[2] = ((LCD_value2 / 10)%10) + '0';
	LCD_string2[1] = ((LCD_value2 / 100)%10) + '0';
	LCD_string2[0] = ((LCD_value2 / 1000)%10) + '0';

	delay_milliseconds(20);
	LCD_u8Write(LCD_string2, 6, 1);
}
